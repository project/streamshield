<?php

namespace Drupal\streamshield\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class StreamshieldContentTypes extends ConfigFormBase {
  public function getFormId() {
    return 'streamshield_content_types';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('streamshield.settings');
    $defaults = $config->get('streamshield.content_types');
    $types = \Drupal::entityTypeManager()->getStorage('node_type')->loadMultiple();
    $commentTypes = \Drupal::entityTypeManager()->getStorage('comment_type')->loadMultiple();
    $types = array_merge($types, $commentTypes);
    $options = [];
    foreach ($types as $type) {
      $options[$type->id()] = ['content_type' => $type->label()];
    }
    $header = ['content_type' => $this->t('Content type')];
    $form['content_types'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $options,
      '#default_value' => $defaults,
      '#empty' => $this->t('No content types found'),
    ];

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('streamshield.settings');

    foreach ($form_state->getValue('content_types') as $content_type => $value) {
      if ($content_type == $value) {
        $config->set('streamshield.content_types.' . $content_type, true);
      } else {
        $config->set('streamshield.content_types.' . $content_type, false);
      }
    }
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  protected function getEditableConfigNames() {
    return [
      'streamshield.settings'
    ];
  }
}