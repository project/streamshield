<?php

namespace Drupal\streamshield\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\InfoParserInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\streamshield\Services\RegistrationService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class StreamshieldRegistration extends ConfigFormBase {

  protected RegistrationService $registrationService;

  public function __construct(ConfigFactoryInterface $config_factory, RegistrationService $registrationService)
  {
    parent::__construct($config_factory, \Drupal::service('config.typed'));
    $this->registrationService = $registrationService;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('registration_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'streamshield_registration';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('streamshield.settings');
    $form['tenant_id'] = [
      '#type' => 'item',
      '#title' => $this->t('Tenant ID'),
      '#description' => $config->get('streamshield.tenant_id'),
    ];
    $form['access_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Streamshield Access Key'),
      '#default_value' => $config->get('streamshield.access_key'),
      '#description' => $this->t('Your Access Key issued by Streamshield.'),
    ];
    $form['secret_key'] = [
      '#type' => 'password',
      '#title' => $this->t('Streamshield Secret Key'),
      '#default_value' => $config->get('streamshield.secret_key'),
      '#description' => $this->t('Your Secret Key issued by Streamshield.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('streamshield.settings');

    try {
      $this->registrationService->register($form_state->getValue('access_key'), $form_state->getValue('secret_key'));
    } catch (\Exception $exception) {
      \Drupal::messenger()->addMessage("That key pair is not authenticated");
    }

    $config->set('streamshield.access_key', $form_state->getValue('access_key'));
    $config->set('streamshield.secret_key', $form_state->getValue('secret_key'));
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'streamshield.settings',
    ];
  }
}