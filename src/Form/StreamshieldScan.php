<?php

namespace Drupal\streamshield\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\streamshield\Services\ModerationService;
use Symfony\Component\DependencyInjection\ContainerInterface;

class StreamshieldScan extends ConfigFormBase {

  protected ModerationService $moderationService;

  public function __construct(ConfigFactoryInterface $config_factory, ModerationService $moderationService)
  {
    parent::__construct($config_factory, \Drupal::service('config.typed'));
    $this->moderationService = $moderationService;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('moderation_service')
    );
  }

  public function getFormId() {
    return 'streamshield_scan';
  }

  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['actions']['submit']['#value'] = $this->t('Scan all content');

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    // TODO use batch api for this
    $nodes = \Drupal::entityTypeManager()->getStorage('node')
      ->loadByProperties(['status' => 1]);

    foreach ($nodes as $node) {
      $this->moderationService->moderate($node);
    }

    return parent::submitForm($form, $form_state);
  }

  protected function getEditableConfigNames() {
    return [
      'streamshield.settings'
    ];
  }
}