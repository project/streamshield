<?php

namespace Drupal\streamshield\Services;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Extension\InfoParserInterface;

class RegistrationService {

  private ClientInterface $httpClient;
  private HashService $hashService;
  private InfoParserInterface $infoParser;

  public function __construct(ClientInterface $client, HashService $hashService, InfoParserInterface $infoParser)
  {
    $this->httpClient = $client;
    $this->hashService = $hashService;
    $this->infoParser = $infoParser;
    $this->config = \Drupal::getContainer()->get('config.factory')->getEditable('streamshield.settings');
  }

  public function register(string $accessKey, string $secretKey): void
  {
    $infoFile = \Drupal::service('extension.list.module')->getPath('streamshield') . '/streamshield.info.yml';
    $info = $this->infoParser->parse($infoFile);
    $data = [
      'cms_name' => 'Drupal',
      'cms_version' => \Drupal::VERSION,
      'plugin_version' => array_key_exists('version', $info)
        ? $info['version']
        : '0.0.1',
    ];

    try {
      $url = $this->buildUrl($data, $accessKey, $secretKey);
      $response = $this->httpClient->post($url, [
        'verify' => false,
        'json' => $data,
        'headers' => ['Accept' => 'application/json', 'Content-Type' => 'application/json']
      ]);
      $responseData = $this->extractResponse((string) $response->getBody());
      $this->config->set('streamshield.tenant_id', $responseData['data']['tenant_id']);
      $this->config->save();
    } catch (\Exception $e) {
      \Drupal::messenger()->addMessage("That key pair is not authenticated");
    }
  }

  private function buildUrl(array $data, $access_key, $secret_key): string {
    $url = $this->getAdminApiUrl() . "/plugins";
    $signature = $this->hashService->generateSignature($data, $secret_key);

    return "$url?access_key=$access_key&signature=$signature";
  }

  private function extractResponse(string $response)
  {
    $data = explode("\n", $response);
    // Json is last item
    return json_decode(end($data), true);
  }

  private function getAdminApiUrl() {
    $host = \Drupal::request()->getHttpHost();
    if (str_contains($host, 'dev.streamshield.ai')) {
      return 'https://api.dev.streamshield.ai';
    } elseif (str_contains($host, 'localhost')) {
      return 'http://host.docker.internal:8080';
    }
    return $this->config->get('streamshield.admin-api');
  }
}
