<?php

namespace Drupal\streamshield\Services;

class HashService {
  public function generateSignature(array $data, $secretKey = null): string {
    $config = \Drupal::config('streamshield.settings');
    $bData = utf8_encode($this->formatdata($data));
    $secret = $this->urlSafeB64Decode($secretKey ?? $config->get('streamshield.secret_key'));
    $hash = hash_hmac('sha256', $bData, $secret, true);
    return strtr(base64_encode($hash), '+/', '-_');
  }

  public function checkHash(string $data, string $accessKey, string $signature): bool {
    $config = \Drupal::config('streamshield.settings');

    if ($accessKey != $config->get('streamshield.access_key')) {
      error_log("Access key mismatch");
      return false;
    }

    $bSignature = $this->urlSafeB64Decode($signature);
    $bData = utf8_encode($data);
    $secret = $this->urlSafeB64Decode($config->get('streamshield.secret_key'));

    $ourSig = hash_hmac('sha256', $bData, $secret, true);

    return $ourSig == $bSignature;
  }

  private function formatdata(array $data): string {
    ksort($data);
    return json_encode($data, JSON_UNESCAPED_SLASHES);
  }

  private function urlSafeB64Decode(string $string): string {
    $data = str_replace(['-', '_'], ['+', '/'], $string);
    $mod4 = strlen($data) % 4;
    if ($mod4) {
      $data .= substr('====', $mod4);
    }
    return base64_decode($data);
  }
}