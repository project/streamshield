<?php

namespace Drupal\streamshield\Services;

use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;
use Drupal\node\Entity\Node;
use Drupal\comment\Entity\Comment;
use Drupal\file\Entity\File;
use Drupal\Core\Url;

class ModerationService {
  protected $httpClient;
  protected $hashService;
  protected $config;

  private static $doNotModerate = [
    'nid', 'uuid', 'vid', 'langcode', 'type', 'revision_timestamp', 'revision_uid', 'revision_log', 'status', 'uid',
    'created', 'changed', 'promote', 'sticky', 'default_langcode', 'revision_default', 'revision_translation_affected',
    'path', 'comment', 'field_tags',
  ];

  private static $doNotModerateComment = [
    'thread', 'entity_type', 'field_name', 'hostname', 'name'
  ];

  private static $stringTypes = [
    'string', 'text_with_summary', 'text_long', 'list_string', 'text', 'string_long'
  ];

  private static $fileTypes = [
    'image', 'file'
  ];

  /**
   * @param ClientInterface $http_client
   */
  public function __construct(ClientInterface $http_client, HashService $hashService) {
    $this->httpClient = $http_client;
    $this->hashService = $hashService;
    $this->config = \Drupal::config('streamshield.settings');
  }

  public function moderate(Node $node, $action='created') {
    $config = \Drupal::config('streamshield.settings');

    if ($config->get('streamshield.access_key') == '' || $config->get('streamshield.secret_key') == '') {
      return;
    }

    if (!$config->get('streamshield.content_types.' . $node->getType())) {
      return;
    }

    $moderationSet = $this->gatherModerationSet($node);
    $user = $this->getCurrentUser();
    $this->addStreamshieldMeta($moderationSet, $node, $user, $action);

    $this->send($moderationSet);
  }

  public function moderateComment(Comment $comment) {
    $config = \Drupal::config('streamshield.settings');

    if ($config->get('streamshield.access_key') == '' || $config->get('streamshield.secret_key') == '') {
      return;
    }

    if (!$config->get('streamshield.content_types.' . $comment->getTypeId())) {
      return;
    }

    $moderationSet = $this->gatherCommentModerationSet($comment);
    $user = $this->getCurrentUser();
    $this->addCommentStreamshieldMeta($moderationSet, $comment, $user);

    $this->send($moderationSet);
  }

  private function send(array $content) {
    try {
      $response = $this->httpClient->request('POST', $this->buildUrl($content), [
        'json' => $content
      ]);
    } catch (RequestException $e) {
      // Fail quietly
    }
  }

  private function getFieldValueKey($keyName, $index, $numVals)
  {
    if ($numVals == 1) {
      return $keyName;
    }

    return $keyName . "_" . $index;
  }

  private function gatherModerationSet(Node $node) {
    $moderation_set = [];
    foreach ($node->toArray() as $item => $value) {
      if (!in_array($item, static::$doNotModerate)) {
        $field = $node->get($item);
        if (in_array($field->getFieldDefinition()->getType(), static::$stringTypes)) {
          foreach ($value as $key => $fieldValue) {
            $fieldKey = $this->getFieldValueKey($item, $key, count($value));
            $moderation_set[$fieldKey] = [
              'value' => $fieldValue['value'],
              'hash' => sha1($fieldValue['value']),
              'type' => 'string',
            ];
          }
        } elseif (in_array($field->getFieldDefinition()->getType(), static::$fileTypes)) {
          foreach ($value as $key => $fieldValue) {
            $file = File::load($fieldValue['target_id']);
            $uri = Url::fromUri($file->getFileUri());
            $path = \Drupal::service('file_system')->realpath($uri->getUri());
            $fieldKey = $this->getFieldValueKey($item, $key, count($value));
            $moderation_set[$fieldKey] = [
              'value' => \Drupal::service('file_url_generator')->generateString($uri->getUri()),
              'hash' => sha1_file($path),
              'type' => 'file',
            ];
          }
        }
      }
    }

    return $moderation_set;
  }

  private function gatherCommentModerationSet(Comment $comment) {
    $moderation_set = [];
    foreach ($comment->toArray() as $item => $value) {
      if (!in_array($item, static::$doNotModerateComment)) {
        $field = $comment->get($item);
        if (in_array($field->getFieldDefinition()->getType(), static::$stringTypes)) {
          foreach ($value as $key => $fieldValue) {
            $fieldKey = $this->getFieldValueKey($item, $key, count($value));
            $moderation_set[$fieldKey] = [
              'value' => $fieldValue['value'],
              'hash' => sha1($fieldValue['value']),
              'type' => 'string',
            ];
          }
        } elseif (in_array($field->getFieldDefinition()->getType(), static::$fileTypes)) {
          $file = File::load($fieldValue['target_id']);
          $uri = Url::fromUri($file->getFileUri());
          $path = \Drupal::service('file_system')->realpath($uri->getUri());
          $fieldKey = $this->getFieldValueKey($item, $key, count($value));
          $moderation_set[$fieldKey] = [
            'value' => \Drupal::service('file_url_generator')->generateString($uri->getUri()),
            'hash' => sha1_file($path),
            'type' => 'file',
          ];
        }
      }
    }

    return $moderation_set;
  }

  private function addStreamshieldMeta(array &$moderationSet, Node $node, string $name, string $action) {
    $moderationSet['streamshield_meta'] = [
      'domain' => \Drupal::request()->getHttpHost(),
      'scheme' => \Drupal::request()->getScheme(),
      'username' => $name,
      'cms' => "Drupal " . \Drupal::VERSION,
      'cms_meta' => [
        'nid' => $node->id(),
      ],
      'content_path' => \Drupal::request()->getSchemeAndHttpHost() . "/node/" . $node->id(),
      'tenant_id' => $this->config->get('streamshield.tenant_id'),
      'access_key' => $this->config->get('streamshield.access_key'),
      'utc_datetime' => gmdate('c'),
      'ip_address' => \Drupal::request()->getClientIp(),
      'action' => $action,
      'status' => 'PENDING',
    ];
  }

  private function addCommentStreamshieldMeta(array &$moderationSet, Comment $comment, string $name) {
    $moderationSet['streamshield_meta'] = [
      'domain' => \Drupal::request()->getHttpHost(),
      'scheme' => \Drupal::request()->getScheme(),
      'username' => $name,
      'cms' => "Drupal " . \Drupal::VERSION,
      'cms_meta' => [
        'cid' => $comment->id(),
      ],
      'content_path' => \Drupal::request()->getSchemeAndHttpHost() . $comment->permalink()->toString(),
      'tenant_id' => $this->config->get('streamshield.tenant_id'),
      'access_key' => $this->config->get('streamshield.access_key'),
      'utc_datetime' => gmdate('c'),
      'ip_address' => \Drupal::request()->getClientIp(),
      'action' => 'created',
      'status' => 'PENDING',
    ];
  }

  private function getCurrentUser() {
    $userCurrent = \Drupal::currentUser();
    if ($userCurrent->isAuthenticated()) {
      return $userCurrent->getAccountName();
    }

    return "Anonymous/Unauthenticated User";
  }

  private function buildUrl(array $content): string {
    $url = $this->getModerationApiUrl();
    $access_key = $this->config->get('streamshield.access_key');
    $signature = $this->hashService->generateSignature($content['streamshield_meta']);

    return "$url?access_key=$access_key&signature=$signature";
  }

  private function getModerationApiUrl() {
    $host = \Drupal::request()->getHttpHost();
    if (str_contains($host, 'dev.streamshield.ai')) {
      return 'https://moderation.dev.streamshield.ai';
    } elseif (str_contains($host, 'localhost')) {
      return 'http://host.docker.internal:6971';
    }
    return $this->config->get('streamshield.moderation-api');
  }
}