<?php

namespace Drupal\streamshield\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\streamshield\Services\HashService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

class ImageController extends ControllerBase {
  protected HashService $hashService;

  public function __construct(HashService $hashService) {
    $this->hashService = $hashService;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('hash_service')
    );
  }
  public function get(Request $request) {
    $file = str_replace(' ', '%20', $request->query->get('file_path', ''));
    if (!$this->hashService->checkHash($file, $request->query->get('access_key', ''), $request->query->get('signature', ''))) {
      return new JsonResponse(new \stdClass(), 403);
    }
    $realPath = DRUPAL_ROOT . urldecode($file);
    $fileData = bin2hex(file_get_contents($realPath));
    return new JsonResponse(['data' => $fileData]);
  }
}