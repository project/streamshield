<?php

namespace Drupal\streamshield\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\streamshield\Services\HashService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Callback Controller class.
 */
class CallbackController extends ControllerBase {
  /**
   * Injected HashService
   *
   * @var \Drupal\streamshield\Services\HashService
   */
  protected HashService $hashService;

  /**
   * Constructor
   *
   * @param \Drupal\streamshield\Services\HashService $hashService
   */
  public function __construct(HashService $hashService) {
    $this->hashService = $hashService;
  }

  /**
   * Handle DI
   *
   * @param ContainerInterface $container Container Interface
   * @return static
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('hash_service'));
  }

  /**
   * @param Request $request
   * @return Response
   */
  public function update(Request $request) {
    $body = json_decode($request->getContent(), true);

    $ourSig = $this->hashService->generateSignature($body);
    if ($ourSig != $request->get('signature', '')) {
      return new Response('', 403);
    }
    if ($body['action'] == 'unpublish') {
      if (array_key_exists('cid', $body['cms_meta'])) {
        $comment = \Drupal::entityTypeManager()->getStorage('comment')->load($body['cms_meta']['cid']);
        $comment->setUnpublished()->save();
      } elseif (array_key_exists('nid', $body['cms_meta'])) {
        $node = \Drupal::entityTypeManager()->getStorage('node')->load($body['cms_meta']['nid']);
        $node->setUnpublished()->save();
      }
    }
    return new Response('', 204);
  }
}
